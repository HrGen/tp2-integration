Développeurs :
Hugo Raoult, Damien Forafo, Lucas Perez

Dockerfile pour l'Application React et Nginx
Le premier script est un Dockerfile qui construit une image Docker pour une application React, puis la déploie à l'aide de Nginx.

Image Node.js: La première partie du Dockerfile construit l'image de l'application React. Elle utilise node:16-alpine comme image de base, copie les fichiers package.json et package-lock.json dans le dossier de travail /app, installe les dépendances avec npm install, puis copie tous les fichiers de l'application dans /app et construit l'application avec npm run build.

Image Nginx: La seconde partie utilise nginx:alpine comme image de base pour déployer l'application. Elle copie le dossier build généré par l'étape précédente dans le répertoire /usr/share/nginx/html de l'image Nginx, expose le port 80 et définit la commande par défaut pour exécuter Nginx en mode daemon.

Script GitLab CI/CD
Le second script est un fichier de configuration pour un pipeline GitLab CI/CD qui définit plusieurs étapes: préparation, construction, test, et déploiement et versionning avec incréments automatisés.

Étape de préparation (prepare_version): Cette étape génère une version de l'application en utilisant le numéro d'itération du pipeline CI (CI_PIPELINE_IID) comme version de patch, et sauvegarde cette version dans version.env pour les étapes suivantes.

Construction (build): Construit l'image Docker de l'application en utilisant l'étape précédente pour tagger l'image avec la version générée. Si le commit courant est taggué, il taggue également l'image comme latest et la pousse vers le registre Docker.

Test (test): Exécute les tests de l'application en utilisant l'image node:16-alpine.

Déploiement (deploy): Cette étape est prévue pour déployer l'application, mais ne contient que l'écho d'un placeholder. C'est ici que vous intégreriez vos scripts de déploiement spécifiques.

Variables d'Environnement et Sécurité
Le script fait usage de variables d'environnement et de secrets pour gérer l'authentification avec le registre Docker et le repository Git, soulignant l'importance de sécuriser ces informations dans un environnement CI/CD.

Intégration avec GitLab
Ce pipeline est spécifiquement conçu pour GitLab CI/CD, utilisant ses fonctionnalités comme les artefacts (pour passer des fichiers entre les étapes) et les conditions only (pour contrôler quand les étapes doivent s'exécuter).

